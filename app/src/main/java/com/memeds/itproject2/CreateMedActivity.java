package com.memeds.itproject2;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.memeds.itproject2.db.MedList;
import com.memeds.itproject2.helper.SetReminders;
import com.memeds.itproject2.obj.Medicine;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.view.View.GONE;

@SuppressLint({ "NewApi", "DefaultLocale" })
public class CreateMedActivity extends Activity implements AdapterView.OnItemSelectedListener,View.OnClickListener{// implements DatePickerDialog.OnDateSetListener{
	Spinner interval;
	Spinner time_unit;
	Spinner times;
	LinearLayout interval_layout,frequency_layout;
	MedList ML;
	TextView CreateTime2,CreateTime3,CreateTime4,CreateTime5,CreateTime6;
	EditText CreateMName, CreateMDes, CreateMCount, CreateMStartDate,
			CreateMTime1, CreateMTime2, CreateMTime3, CreateMTime4, CreateMStartDate2,
			CreateMDosage;
	//CheckBox CreateMTime1Check, CreateMTime2Check, CreateMTime3Check, CreateMTime4Check;
	Spinner mySpinner ;
	String[] Istrings = { "pill1", "pill2", "pill3", "pill4" };
	int arr_images[] = { R.drawable.pill01, R.drawable.pill02, R.drawable.pill03, R.drawable.pill04};

	Date startDate, endDate;
	String pattern = "MM-dd-yyyy";
	SimpleDateFormat sdf = new SimpleDateFormat(pattern);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_med);
		interval = (Spinner) findViewById(R.id.interval);
		interval_layout = findViewById(R.id.interval_layout);
		CreateMName = (EditText) findViewById(R.id.CreateMName);
		CreateMDes = (EditText) findViewById(R.id.CreateMDes);
		CreateMStartDate = (EditText) findViewById(R.id.CreateMStartDate);
		CreateMCount = (EditText) findViewById(R.id.CreateMCount);
		CreateMTime1 = (EditText) findViewById(R.id.CreateMTime1);
		//CreateMTime1Check = (CheckBox)findViewById(R.id.CreateMTime1Check);

		CreateTime2 = findViewById(R.id.CreateMTime2);
		CreateTime3 = findViewById(R.id.CreateMTime3);
		CreateTime4 = findViewById(R.id.CreateMTime4);
		CreateTime5 = findViewById(R.id.CreateMTime5);
		CreateTime6 = findViewById(R.id.CreateMTime6);

		CreateMStartDate2 = (EditText) findViewById(R.id.CreateMStartDate2);
		CreateMDosage = (EditText) findViewById(R.id.CreateMDosage);

		mySpinner = (Spinner) findViewById(R.id.CreateMSpinnerIcon);
		mySpinner.setAdapter(new MyAdapter(CreateMedActivity.this, R.layout.iconspinner, Istrings));

		Button back = (Button) findViewById(R.id.back);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(CreateMedActivity.this, TrackerActivity.class);
				startActivity(intent);
			}
		});

		ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
				R.array.interval, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		interval.setAdapter(adapter1);

		//Settings listeners
		interval.setOnItemSelectedListener(this);
		CreateMTime1.setOnClickListener(this);
		CreateMStartDate.setOnClickListener(this);
		CreateMStartDate2.setOnClickListener(this);


		Disable();

}



	ArrayList ar;
	DatePickerDialog mDatePicker;
	int mYear = 0;
	int mMonth = 0;
	int mDay = 0;
	DatePickerDialog mDatePicker2;
	int mYear1 = 0;
	int mMonth1 = 0;
	int mDay1 = 0;

	int mYear2 = 0;
	int mMonth2 = 0;
	int mDay2 = 0;

	int mYear3 = 0;
	int mMonth3 = 0;
	int mDay3 = 0;

	int currentTime = 0;
	int currentMin = 0;
	static int hr;


	int hour = 0;
	int minute = 0;

	int hour1 = 0;
	int minute1 = 0;

	int hour3 = 0;
	int minute3 = 0;

	int hourres = 0;
	int minuteres = 0;
	int interval1 = 0;



	@Override
	public void onClick(View v) {

			if (v == CreateMTime1) {
				try {
				final TextView clickedView = (TextView) v;
				final Calendar mcurrentTime = Calendar.getInstance();
				hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
				minute = mcurrentTime.get(Calendar.MINUTE);

				hour1 = hour;
				minute1 = minute;

				TimePickerDialog mTimePicker;
				mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
					@Override
					public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
						boolean isPM = (selectedHour >= 12);
						clickedView.setText(String.format("%02d:%02d %s", (selectedHour == 12 || selectedHour == 0) ? 12 : selectedHour % 12, selectedMinute, isPM ? "PM" : "AM"));

//						clickedView.setText(String.format("%02d", selectedHour) + ":" + String.format("%02d", selectedMinute));
						hour1 = selectedHour;
						minute1 = selectedMinute;

/*
						final Calendar mcurrentTime = Calendar.getInstance();
						mcurrentTime.set(Calendar.HOUR_OF_DAY, hour1);
						mcurrentTime.set(Calendar.MINUTE, minute1);
						hour3 = mcurrentTime.get(Calendar.HOUR_OF_DAY);
						minute3 = mcurrentTime.get(Calendar.MINUTE);
						String zero = "00";
						String one = "01";
						String two = "02";
						String three = "03";
						String four = "04";
						String five = "05";
						String six = "06";
						String seven = "07";
						String eight = "08";
						String nine = "09";
						if (interval.getSelectedItem().toString().equals("4")) {
							if (hour1 == Integer.parseInt(zero)) {
								String hourSet2 = "08";
								String hourSet3 = "12";
								String hourSet4= "16";
								String hourSet5 = "20";
								String hourSet6 = "24";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "04";

									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);

								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "04";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "04";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "04";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "04";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "04";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "04";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "04";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "04";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "04";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else {
									String hourSet = "04";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if (hour1 == Integer.parseInt(one)) {
								String hourSet2 = "09";
								String hourSet3 = "13";
								String hourSet4 = "17";
								String hourSet5 = "21";

								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "05";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "05";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "05";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "05";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "05";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "05";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "05";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "05";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "05";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "05";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else {
									String hourSet = "05";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if (hour1 == Integer.parseInt(two)) {
								String hourSet2 = "10";
								String hourSet3 = "14";
								String hourSet4 = "18";
								String hourSet5 = "22";

								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "06";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "06";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "06";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "06";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "06";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "06";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "06";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "06";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "06";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "06";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else {
									String hourSet = "06";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if (hour1 == Integer.parseInt(three)) {
								String hourSet2 = "11";
								String hourSet3 = "15";
								String hourSet4 = "19";
								String hourSet5 = "23";


								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "07";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "07";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "07";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "07";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "07";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "07";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "07";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "07";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "07";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "07";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else {
									String hourSet = "07";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if (hour1 == Integer.parseInt(four)) {
								String hourSet2 = "12";
								String hourSet3 = "16";
								String hourSet4 = "20";
								String hourSet5 = "00";

								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "08";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "08";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "08";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "08";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "08";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "08";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "08";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "08";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "08";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "08";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else {
									String hourSet = "08";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if (hour1 == Integer.parseInt(five)) {
								String hourSet2 = "13";
								String hourSet3 = "17";
								String hourSet4 = "21";
								String hourSet5 = "01";

								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "09";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "09";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "09";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "09";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "09";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "09";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "09";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "09";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "09";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "09";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else {
									String hourSet = "09";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if (hour1 == Integer.parseInt(six)) {
								String hourSet2 = "14";
								String hourSet3 = "18";
								String hourSet4 = "22";
								String hourSet5 = "02";

								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "10";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "10";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "10";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "10";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "10";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "10";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "10";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "10";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "10";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "10";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else {
									String hourSet = "10";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if (hour1 == Integer.parseInt(seven)) {
								String hourSet2 = "15";
								String hourSet3 = "19";
								String hourSet4 = "23";
								String hourSet5 = "03";

								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "11";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "11";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "11";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "11";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "11";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "11";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "11";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "11";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "11";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "11";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else {
									String hourSet = "11";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if (hour1 == Integer.parseInt(eight)) {
								String hourSet2 = "16";
								String hourSet3 = "20";
								String hourSet4 = "00";
								String hourSet5 = "04";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "12";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "12";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "12";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "12";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "12";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "12";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "12";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "12";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "12";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "12";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else {
									String hourSet = "12";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if (hour1 == Integer.parseInt(nine)) {
								String hourSet2 = "17";
								String hourSet3 = "21";
								String hourSet4 = "01";
								String hourSet5 = "05";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "13";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "13";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "13";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "13";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "13";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "13";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "13";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "13";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "13";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "13";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else {
									String hourSet = "13";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}

							}
							else if(hour1==10){
								String hourSet2 = "18";
								String hourSet3 = "22";
								String hourSet4 = "02";
								String hourSet5 = "06";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "14";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "14";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "14";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "14";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "14";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "14";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "14";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);;
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "14";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "14";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "14";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								}
								else {
									String hourSet = "14";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if(hour1==11){
								String hourSet2 = "19";
								String hourSet3 = "23";
								String hourSet4 = "03";
								String hourSet5 = "07";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "15";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "15";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "15";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "15";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "15";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "15";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "15";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "15";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "15";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "15";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								}
								else {
									String hourSet = "15";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if(hour1==12){
								String hourSet2 = "20";
								String hourSet3 = "00";
								String hourSet4 = "04";
								String hourSet5 = "08";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "16";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "16";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "16";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "16";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "16";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "16";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "16";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "16";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "16";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "16";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								}
								else {
									String hourSet = "16";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if(hour1==13){
								String hourSet2 = "21";
								String hourSet3 = "01";
								String hourSet4 = "05";
								String hourSet5 = "09";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "17";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "17";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "17";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "17";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "17";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "17";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "17";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "17";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "17";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "17";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								}
								else {
									String hourSet = "17";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if(hour1==14){
								String hourSet2 = "22";
								String hourSet3 = "02";
								String hourSet4 = "06";
								String hourSet5 = "10";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "18";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "18";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "18";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "18";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "18";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "18";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "18";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "18";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "18";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "18";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								}
								else {
									String hourSet = "18";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);

								}
							}
							else if(hour1==15){
								String hourSet2 = "23";
								String hourSet3 = "03";
								String hourSet4 = "07";
								String hourSet5 = "11";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "19";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "19";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "19";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "19";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "19";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "19";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "19";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "19";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "19";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "19";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								}
								else {
									String hourSet = "19";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if(hour1==16){
								String hourSet2 = "00";
								String hourSet3 = "04";
								String hourSet4 = "08";
								String hourSet5 = "12";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "20";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "20";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "20";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "20";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "20";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "20";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "20";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "20";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "20";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "20";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								}
								else {
									String hourSet = "20";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if(hour1==17){
								String hourSet2 = "01";
								String hourSet3 = "05";
								String hourSet4 = "09";
								String hourSet5 = "13";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "21";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "21";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "21";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "21";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "21";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "21";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "21";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "21";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "21";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "21";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								}
								else {
									String hourSet = "21";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if(hour1==18){
								String hourSet2 = "02";
								String hourSet3 = "06";
								String hourSet4 = "10";
								String hourSet5 = "14";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "22";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "22";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "22";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "22";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "22";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "22";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "22";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "22";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "22";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "22";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								}
								else {
									String hourSet = "22";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if(hour1==19){
								String hourSet2 = "03";
								String hourSet3 = "07";
								String hourSet4 = "11";
								String hourSet5 = "15";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "23";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "23";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "23";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "23";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "23";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "23";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "23";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "23";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "23";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "23";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								}
								else {
									String hourSet = "23";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if(hour1==20){
								String hourSet2 = "04";
								String hourSet3 = "08";
								String hourSet4 = "12";
								String hourSet5 = "16";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "00";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "00";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "00";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "00";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "00";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "00";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "00";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "00";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "00";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "00";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								}
								else {
									String hourSet = "00";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if(hour1==21){
								String hourSet2 = "05";
								String hourSet3 = "09";
								String hourSet4 = "13";
								String hourSet5 = "17";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "01";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "01";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "01";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "01";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "01";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "01";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "01";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "01";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "01";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "01";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								}
								else {
									String hourSet = "01";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if(hour1==22){
								String hourSet2 = "06";
								String hourSet3 = "10";
								String hourSet4 = "14";
								String hourSet5 = "18";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "02";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "02";
									String minuteSet = "01";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "02";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "02";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "02";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "02";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "02";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "02";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "02";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "02";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								}
								else {
									String hourSet = "02";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}
							else if(hour1==23){
								String hourSet2 = "07";
								String hourSet3 = "11";
								String hourSet4 = "15";
								String hourSet5 = "19";
								if (minute1 == Integer.parseInt(zero)) {
									String hourSet = "03";
									String minuteSet = "00";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(one)) {
									String hourSet = "03";
									String minuteSet = "01";
									CreateMTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(two)) {
									String hourSet = "03";
									String minuteSet = "02";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(three)) {
									String hourSet = "03";
									String minuteSet = "03";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(four)) {
									String hourSet = "03";
									String minuteSet = "04";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(five)) {
									String hourSet = "03";
									String minuteSet = "05";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(six)) {
									String hourSet = "03";
									String minuteSet = "06";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(seven)) {
									String hourSet = "03";
									String minuteSet = "07";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(eight)) {
									String hourSet = "03";
									String minuteSet = "08";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								} else if (minute1 == Integer.parseInt(nine)) {
									String hourSet = "03";
									String minuteSet = "09";
									CreateTime2.setText(hourSet + ":" + minuteSet);
									CreateTime3.setText(hourSet2+ ":" +minuteSet);
									CreateTime4.setText(hourSet3+ ":" +minuteSet);
									CreateTime5.setText(hourSet4+ ":" +minuteSet);
									CreateTime6.setText(hourSet5+ ":" +minuteSet);
								}
								else {
									String hourSet = "03";
									CreateTime2.setText(hourSet + ":" + minute3);
									CreateTime3.setText(hourSet2+ ":" +minute3);
									CreateTime4.setText(hourSet3+ ":" +minute3);
									CreateTime5.setText(hourSet4+ ":" +minute3);
									CreateTime6.setText(hourSet5+ ":" +minute3);
								}
							}*/
						//}


					}
				}, hour, minute, false);//Yes 24 hour time
				mTimePicker.setTitle("Select Time\n");
				mTimePicker.show();
			}
		catch (Exception e)
			{}
			}


			else {
				if (v == CreateMStartDate) {
					final TextView clickedView = (TextView) v;
					final Calendar mcurrentDate = Calendar.getInstance();
					mYear = mcurrentDate.get(Calendar.YEAR);
					mMonth = mcurrentDate.get(Calendar.MONTH);
					mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

					mMonth2 = mMonth;
					mYear2 = mYear;
					mDay2 = mDay;
					//Calendar.add(Calendar.DATE,0);
					//mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
					mDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

						public void onDateSet(DatePicker datepicker, int year, int monthOfYear, int dayOfMonth) {
							clickedView.setText(String.format("%02d", dayOfMonth) + "/" + String.format("%02d", monthOfYear + 1) + "/"
									+ String.format("%04d", year));

							try {
								startDate = sdf.parse(monthOfYear + "-" + dayOfMonth +"-"+ year);
								Log.e("AAAAA", "onDateSet: " + startDate );
							}catch (Exception exception){
								Log.e("AAAAA", "onDateSet: exception");
							}

							mYear2 = year;
							mMonth2 = monthOfYear;
							mDay2 = dayOfMonth;
						}
					}, mYear, mMonth, mDay);
					mDatePicker.getDatePicker().setMinDate(new Date().getTime());
					mDatePicker.setTitle("Select Start Date");
					mDatePicker.show();
					//if(clickedView.getText().length()!=0)
						CreateMStartDate2.setEnabled(true);

				} else if (v == CreateMStartDate2) {
					final TextView clickedView = (TextView) v;
					final Calendar mcurrentDate = Calendar.getInstance();
					mYear1 = mcurrentDate.get(Calendar.YEAR);
					mMonth1 = mcurrentDate.get(Calendar.MONTH);
					mDay1 = mcurrentDate.get(Calendar.DAY_OF_MONTH);

					mMonth3 = mMonth1;
					mYear3 = mYear1;
					mDay3 = mDay1;
					mDatePicker2 = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
						public void onDateSet(DatePicker datepicker, int year, int monthOfYear, int dayOfMonth) {

							clickedView.setText(String.format("%02d", dayOfMonth) + "/" + String.format("%02d", monthOfYear + 1) + "/"
									+ String.format("%04d", year));
							try {
								endDate = sdf.parse(monthOfYear + "-" + dayOfMonth +"-"+ year);
								Log.e("AAAAA", "onDateSet: " + endDate );
							}catch (Exception exception){
								Log.e("AAAAA", "onDateSet: exception");
							}

							if(startDate.before(endDate) || startDate.equals(endDate)){
								mYear3 = year;
								mMonth3 = monthOfYear;
								mDay3 = dayOfMonth;
							}
							else{
								Toast.makeText(CreateMedActivity.this, "End Date cannot be earlier than Start Date", Toast.LENGTH_LONG).show();
								clickedView.setText("");
								}
							}
					}, mYear1, mMonth1, mDay1);
					mDatePicker2.getDatePicker().setMinDate(new Date().getTime());
					mDatePicker2.setTitle("Select End Date");
					mDatePicker2.show();
					interval.setEnabled(true);
					CreateMTime1.setEnabled(true);


				}
			}
	}


	/*DatePickerDialog mDatePicker1;
	int mYear1 = 0;
	int mMonth1 = 0;
	int mDay1 = 0;
	int mYear2 = 0;
	int mMonth2 = 0;
	int mDay2 = 0;

	@Override
	public void onClick(View v) {
		if (v == CreateMStartDate) {
			final Calendar mcurrentDate = Calendar.getInstance();
			mYear1 = mcurrentDate.get(Calendar.YEAR);
			mMonth1 = mcurrentDate.get(Calendar.MONTH);
			mDay1 = mcurrentDate.get(Calendar.DAY_OF_MONTH);

			mMonth2 = mMonth1;
			mYear2 = mYear1;
			mDay2 = mDay1;
			mDatePicker1 = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
				public void onDateSet(DatePicker datepicker, int year, int monthOfYear, int dayOfMonth) {
					CreateMStartDate.setText(String.format("%02d", monthOfYear + 1) + "/" + String.format("%02d", dayOfMonth) + "/"
							+ String.format("%04d", year));
					mYear2 = year;
					mMonth2 = monthOfYear;
					mDay2 = dayOfMonth;
				}
			}, mYear1, mMonth1, mDay1);

			mDatePicker1.setTitle("Select date");
			mDatePicker1.show();
		}
	}*/
	/*
	public void showDatePickerDialog(){
		DatePickerDialog datePickerDialog = new DatePickerDialog(
				this,
				this,
				Calendar.getInstance().get(Calendar.YEAR),
				Calendar.getInstance().get(Calendar.MONTH),
				Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		datePickerDialog.show();
	}

	@Override
	public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
		String date = month + 1+"/" + dayOfMonth + "/" + year;
		CreateMStartDate.setText(date);
	}
*/
	public class MyAdapter extends ArrayAdapter<String>{

        public MyAdapter(Context context, int textViewResourceId,   String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView,ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater=getLayoutInflater();
            View row=inflater.inflate(R.layout.iconspinner, parent, false);

            ImageView icon=(ImageView)row.findViewById(R.id.iconimage);
            icon.setImageResource(arr_images[position]);

            return row;
            }
        }

	public void CreateMed(View v) {
		String a = CreateMName.getText().toString().trim();
		String b = CreateMDes.getText().toString().trim();
		String c = CreateMCount.getText().toString().trim();
		String d = CreateMStartDate.getText().toString().trim();
		String e = CreateMStartDate2.getText().toString().trim();
		String f = CreateMTime1.getText().toString().trim();
		String g = CreateMDosage.getText().toString().trim();
		String h = "";
		if(mySpinner.getSelectedItem().toString()==Istrings[0]||mySpinner.getSelectedItem().toString()==Istrings[1])
		{
			h = c+"mg";
		}
		else if(mySpinner.getSelectedItem().toString()==Istrings[2])
		{
			h = c+"ml";
		}
		else if(mySpinner.getSelectedItem().toString()==Istrings[3])
		{
			h = c+"cc";
		}
		if(c.equals("0")||c.equals("00")||c.equals("000")||c.equals("0000")||c.equals("00000"))
		{
			Toast.makeText(this, "Stock cannot be 0", Toast.LENGTH_SHORT).show();
		}

 		else if(TextUtils.isEmpty(a)||TextUtils.isEmpty(b)||TextUtils.isEmpty(c)||TextUtils.isEmpty(d)||TextUtils.isEmpty(e)||TextUtils.isEmpty(f)||TextUtils.isEmpty(g))
		{
			Toast.makeText(this, "Please Insert All Fields", Toast.LENGTH_SHORT).show();
		}
		 else {
			Medicine M = new Medicine();
			M.NAME = CreateMName.getText().toString();
			M.DESCRIPTION = CreateMDes.getText().toString();
			M.STARTDATE = CreateMStartDate.getText().toString();
			try {
				M.COUNT = Integer.parseInt(CreateMCount.getText().toString());
			} catch (Exception ex) {
				M.COUNT = 5;
			}
			//if (CreateMTime1Check.isChecked())
				M.TIME1 = CreateMTime1.getText().toString();
			M.TIME2 = CreateTime2.getText().toString();
			M.TIME3 = CreateTime3.getText().toString();
			M.TIME4 = CreateTime4.getText().toString();
			M.TIME5 = CreateTime5.getText().toString();
			M.TIME6 = CreateTime6.getText().toString();


			M.IMAGE = mySpinner.getSelectedItemPosition() + 1;


			M.STARTDATE2 = CreateMStartDate2.getText().toString();
			M.DOSAGE = h;
			//M.DOSAGE = CreateMDosage.getText().toString();
			M.MED_INTERVAL = Integer.parseInt(interval.getSelectedItem().toString());



			ML = new MedList(this.getBaseContext());
			ML.openWritable();
			long id = ML.insertData(M);
			ML.close();
			M.MED_ID = (int) id;

			SetReminders.Set(this, M);

			Intent intent2 = new Intent(this, TrackerActivity.class);
			intent2.putExtra("from", "1");
			startActivity(intent2);
			this.finish();
		}
	}

	

/*

	public void showDatePickerDialog(View v) {
    DialogFragment newFragment = new DatePickerFragment();
    newFragment.show(getFragmentManager(), "datePicker");
}

public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		EditText et = (EditText) getActivity().findViewById(R.id.CreateMStartDate);
		String time = et.getText().toString();
		if (!time.isEmpty()) {
			try {
				String[] sp = time.split("/");
				year = Integer.parseInt(sp[2]);
				month = Integer.parseInt(sp[1]) - 1;
				day = Integer.parseInt(sp[0]);
			} catch (Exception ex) {
			}
		}
		return new DatePickerDialog(getActivity(), this, year, month, day);
	}

	@Override
	public void onDateSet(DatePicker view, int year, int month, int day) {
		String a = String.format("%02d", day);
		String b = String.format("%02d", month + 1);
		String c = String.format("%04d", year);
		EditText dateValue = (EditText) getActivity().findViewById(R.id.CreateMStartDate);
		dateValue.setText(a + "/" + b + "/" + c);
	}
}
*/
/*
	public void showDatePickerDialog(View v) {
		if(v==CreateMStartDate) {
			CreateMStartDate.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					DialogFragment newFragment = new DatePickerFragment();
					newFragment.show(getFragmentManager(), "datePicker");
				}
			});
		}
		else if(v==CreateMStartDate2) {
			CreateMStartDate2.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					DialogFragment newFragment = new DatePickerFragment2();
					newFragment.show(getFragmentManager(), "datePicker");
				}
			});
		}
	}

	//@SuppressLint("ValidFragment")
	public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);


			//Calendar.add(Calendar.DATE,0);
			//Date newDate = Calendar.getTime();
			//DatePickerDialog.getDatePicker().setMinDate(newDate.getTime()-(newDate.getTime()%(24*60*60*1000)));
			EditText et = (EditText) getActivity().findViewById(R.id.CreateMStartDate);
			String time = et.getText().toString();
			if (!time.isEmpty()) {
				try {
					String[] sp = time.split("/");
					year = Integer.parseInt(sp[2]);
					month = Integer.parseInt(sp[1]) - 1;
					day = Integer.parseInt(sp[0]);
				} catch (Exception ex) {
				}
			}
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			String a = String.format("%02d", day);
			String b = String.format("%02d", month + 1);
			String c = String.format("%04d", year);
			EditText dateValue = (EditText) getActivity().findViewById(R.id.CreateMStartDate);
			dateValue.setText(a + "/" + b + "/" + c);
		}
	}

	//@SuppressLint("ValidFragment")
	public static class DatePickerFragment2 extends DialogFragment implements DatePickerDialog.OnDateSetListener {

		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			EditText et = (EditText) getActivity().findViewById(R.id.CreateMStartDate2);
			String time = et.getText().toString();
			if (!time.isEmpty()) {
				try {
					String[] sp = time.split("/");
					year = Integer.parseInt(sp[2]);
					month = Integer.parseInt(sp[1]) - 1;
					day = Integer.parseInt(sp[0]);
				} catch (Exception ex) {
				}
			}
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			String a = String.format("%02d", day);
			String b = String.format("%02d", month + 1);
			String c = String.format("%04d", year);
			EditText dateValue = (EditText) getActivity().findViewById(R.id.CreateMStartDate2);
			dateValue.setText(a + "/" + b + "/" + c);
		}
	}
*/
/*
public void showTimePickerDailog(View v){
	DialogFragment newFragment = new TimePickerFragment(v.getId());
    newFragment.show(getFragmentManager(), "timePicker");
}
@SuppressLint("ValidFragment")
public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{

	int ID;
	int CheckID;
	public TimePickerFragment(int id){
		ID = id;
		switch (ID) {
		case R.id.CreateMTime1:
			CheckID = R.id.CreateMTime1Check;
			break;
		default:
			CheckID = R.id.CreateMTime1Check;
			break;
		}
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		int hour = 0;
    	int min = 0;
		EditText et = (EditText)getActivity().findViewById(ID);
        String time = et.getText().toString();
        if(!time.isEmpty()){
        	try {
        	String[] sp = time.split(":");
        	hour = Integer.parseInt(sp[0]);
        	min = Integer.parseInt(sp[1]);
        	}catch(Exception ex){}
        }else{
        	final Calendar c = Calendar.getInstance();
        	hour = c.get(Calendar.HOUR_OF_DAY);
        	min = c.get(Calendar.MINUTE);
        }
		return new TimePickerDialog(getActivity(),this,hour,min,true);
	}
	
	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		// TODO Auto-generated method stub
		String a = String.format("%02d",hourOfDay);
        String b = String.format("%02d",minute);       
        EditText dateValue = (EditText)getActivity().findViewById(ID);
        dateValue.setText(a + ":" + b);
        CheckBox cb =(CheckBox)getActivity().findViewById(CheckID);
        cb.setChecked(true);
			}

	
}
	*/
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
	}

@Override
public void onBackPressed() {
	Intent intent = new Intent(this, TrackerActivity.class);
	startActivity(intent);
	this.finish();
	super.onBackPressed();
}

public void Disable()
{
    CreateMStartDate2.setEnabled(false);
    interval.setEnabled(false);
    CreateMTime1.setEnabled(false);
}
}
