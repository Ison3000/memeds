package com.memeds.itproject2;

import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

@SuppressWarnings("deprecation")
public class TrackerActivity extends TabActivity {
	ImageView tlb3;
	String phone;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tracker);

		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"UserDetails", MODE_PRIVATE);
		phone = pref.getString("RPhone", null);
		tlb3 = findViewById(R.id.t1b3);
		tlb3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_DIAL);
				intent.setData(Uri.parse("tel:"+phone));
				startActivity(intent);
			}
		});
		
		TabHost th = getTabHost();
		Intent intent = new Intent(this , MedicineActivity.class);
		
		TabSpec ts = th.newTabSpec("SCHEDULES");
		ts.setContent(intent);
		ts.setIndicator("", getResources().getDrawable(R.drawable.schedule));
		th.addTab(ts);

		intent = new Intent(this , HistoryActivity.class);
		
		ts = th.newTabSpec("HISTORY");
		ts.setContent(intent);
		ts.setIndicator("", getResources().getDrawable(R.drawable.history));
		th.addTab(ts);

		intent = new Intent(this , SettingsActivity.class);

		ts = th.newTabSpec("SETTINGS");
		ts.setContent(intent);
		ts.setIndicator("", getResources().getDrawable(R.drawable.setting));
		th.addTab(ts);
	}
}
