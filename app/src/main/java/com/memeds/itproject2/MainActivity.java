package com.memeds.itproject2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	String RPassCode, PassCode = "";
	String Rhint;
	ImageView PasscodeImg;
	int passPosition = 0;
	//TextView sample;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		//sample = findViewById(R.id.sample);
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"UserDetails", MODE_PRIVATE);
		RPassCode = pref.getString("RPasscode", null);
		Rhint = pref.getString("RUserName", null);

		//sample.setText(RPassCode);
		if (RPassCode == null) {
			Intent intent = new Intent(this, RegistrationActivity.class);
			startActivity(intent);
			this.finish();
		}

		PasscodeImg = (ImageView) findViewById(R.id.passcodeimg1);
	}

	public void passCodeBtnClick(View v) {
		if (passPosition < 4) {
			passPosition = passPosition + 1;
			Button key = (Button) findViewById(v.getId());
			PassCode = PassCode + key.getText().toString();
			int drawableResourceId = this.getResources().getIdentifier(
					"imgp" + passPosition, "drawable", this.getPackageName());
			PasscodeImg.setImageResource(drawableResourceId);

			if (passPosition == 4) {
				if (PassCode.equals(RPassCode)) {
					Intent intent = new Intent(this, TrackerActivity.class);
					startActivity(intent);
					finish();
				} else {
					passPosition = 0;
					PassCode = "";
					Handler handler = new Handler(); 
				    handler.postDelayed(new Runnable() { 
				         public void run() { PasscodeImg.setImageResource(R.drawable.imgp); 
				         } 
				    }, 500);
					Toast.makeText(this, "Incorrect Passcode!", Toast.LENGTH_LONG).show();

				}
			}

		} else {
			passPosition = 0;
			PassCode = "";
			PasscodeImg.setImageResource(R.drawable.imgp);
		}
	}
/*
	public void backspace(View w)
	{
		String bspace = PassCode;
		if(bspace.length()==1)
		{
				passPosition = passPosition - 1;
				int drawableResourceId = this.getResources().getIdentifier(
						"imgp" + passPosition, "drawable", this.getPackageName());
				PasscodeImg.setImageResource(drawableResourceId);
				//bspace = bspace.substring(0,bspace.length()-1);

		}
		else if(bspace.length()==2)
		{
			passPosition = passPosition - 1;
			int drawableResourceId = this.getResources().getIdentifier(
					"imgp" + passPosition, "drawable", this.getPackageName());
			PasscodeImg.setImageResource(drawableResourceId);
		}
		else if(bspace.length()==3)
		{
			passPosition = passPosition - 1;
			int drawableResourceId = this.getResources().getIdentifier(
					"imgp" + passPosition, "drawable", this.getPackageName());
			PasscodeImg.setImageResource(drawableResourceId);
		}
	}

	public void clearbtn(View w)
	{
		String bspace = PassCode;
		if(bspace.length()>0)
		{
			passPosition = passPosition - 3;
			int drawableResourceId = this.getResources().getIdentifier(
					"imgp" + passPosition, "drawable", this.getPackageName());
			PasscodeImg.setImageResource(drawableResourceId);
		}
		/*if(bspace.length()==1)
		{
			passPosition = passPosition - 1;
			int drawableResourceId = this.getResources().getIdentifier(
					"imgp" + passPosition, "drawable", this.getPackageName());
			PasscodeImg.setImageResource(drawableResourceId);
		}
		else if(bspace.length()==2)
		{
			passPosition = passPosition - 2;
			int drawableResourceId = this.getResources().getIdentifier(
					"imgp" + passPosition, "drawable", this.getPackageName());
			PasscodeImg.setImageResource(drawableResourceId);
		}
		else if(bspace.length()==3)
		{
			passPosition = passPosition - 3;
			int drawableResourceId = this.getResources().getIdentifier(
					"imgp" + passPosition, "drawable", this.getPackageName());
			PasscodeImg.setImageResource(drawableResourceId);
		}
	}*/
	public void forgotPasswordBtnClick(View v){
		//Intent intent = new Intent(this, ForgotPasswordActivity.class);
		//startActivity(intent);

		Toast.makeText(this, "Your Hint is "+Rhint, Toast.LENGTH_SHORT).show();
	}


}
