package com.memeds.itproject2;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import com.memeds.itproject2.helper.DailogChangePassword;
import com.memeds.itproject2.helper.DailogChangePhone;

public class SettingsActivity extends PreferenceActivity {

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		//setContentView(R.xml.preferences);
		//populateSpinners();
		Preference button = (Preference)getPreferenceManager().findPreference("changepassword");      
	    if (button != null) {
	        button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {
	            	DailogChangePassword.app_launched(SettingsActivity.this);   
	                return true;
	            }
	        });     
	    }

		Preference button1 = (Preference)getPreferenceManager().findPreference("changephone");
		if (button1 != null) {
			button1.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
				@Override
				public boolean onPreferenceClick(Preference arg0) {
					DailogChangePhone.app_launched(SettingsActivity.this);
					return true;
				}
			});
		}
	}
}