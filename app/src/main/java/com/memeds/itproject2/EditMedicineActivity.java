package com.memeds.itproject2;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.memeds.itproject2.db.MedList;
import com.memeds.itproject2.helper.SetReminders;
import com.memeds.itproject2.obj.Medicine;

import java.util.Calendar;

@SuppressLint({ "NewApi", "DefaultLocale" })
public class EditMedicineActivity extends Activity {

	long id;
	MedList ML;
	EditText EditMName, EditMDes, EditMCount, EditMStartDate, EditMTime1,
			EditMTime2, EditMTime3, EditMTime4, EditMStartDate2, EditMDosage;
	//CheckBox EditMTime1Check, EditMTime2Check, EditMTime3Check,
	//		EditMTime4Check;
	Spinner mySpinner;
	String[] Istrings = {"pill1", "pill2","pill3", "pill4"};
	int arr_images[] = {R.drawable.pill01, R.drawable.pill02,R.drawable.pill03, R.drawable.pill04};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_medicine);

		EditMName = (EditText) findViewById(R.id.EditMName);
		EditMDes = (EditText) findViewById(R.id.EditMDes);
		EditMStartDate = (EditText) findViewById(R.id.EditMStartDate);
		EditMCount = (EditText) findViewById(R.id.EditMCount);
		EditMTime1 = (EditText) findViewById(R.id.EditMTime1);

		//EditMTime1Check = (CheckBox) findViewById(R.id.EditMTime1Check);

		EditMStartDate2 = (EditText) findViewById(R.id.EditMStartDate2);
		EditMDosage = (EditText) findViewById(R.id.EditMDosage);

		mySpinner = (Spinner) findViewById(R.id.EditMSpinnerIcon);
		mySpinner.setAdapter(new MyAdapter(EditMedicineActivity.this,
				R.layout.iconspinner, Istrings));

        EditMStartDate.setEnabled(false);
        EditMTime1.setEnabled(false);
        //mySpinner.setEnabled(false);

		Intent mIntent = getIntent();
		id = mIntent.getLongExtra("id", 0);
		// id = getIntent().getExtras().getInt("id");
		// Toast.makeText(this, "id:"+intValue,Toast.LENGTH_LONG).show();

		MedList ML = new MedList(this.getBaseContext());
		ML.openReadable();
		Medicine md = ML.getMedDetailsObj(id);
		ML.close();

		EditMName.setText(md.NAME);
		EditMDes.setText(md.DESCRIPTION);
		EditMStartDate.setText(md.STARTDATE);
		EditMStartDate2.setText(md.STARTDATE2);
		EditMDosage.setText(md.DOSAGE);

		EditMCount.setText("" + md.COUNT);
		EditMTime1.setText(md.TIME1);
		//EditMTime1Check.setChecked(md.TIME1 != null && !md.TIME1.isEmpty());
		mySpinner.setSelection(md.IMAGE - 1);


		Button back = (Button) findViewById(R.id.back1);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(EditMedicineActivity.this, TrackerActivity.class);
				startActivity(intent);
			}
		});
	}

	public class MyAdapter extends ArrayAdapter<String> {

		public MyAdapter(Context context, int textViewResourceId,
						 String[] objects) {
			super(context, textViewResourceId, objects);
		}

		@Override
		public View getDropDownView(int position, View convertView,
									ViewGroup parent) {
			return getCustomView(position, convertView, parent);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			return getCustomView(position, convertView, parent);
		}

		public View getCustomView(int position, View convertView,
								  ViewGroup parent) {

			LayoutInflater inflater = getLayoutInflater();
			View row = inflater.inflate(R.layout.iconspinner, parent, false);

			ImageView icon = (ImageView) row.findViewById(R.id.iconimage);
			icon.setImageResource(arr_images[position]);

			return row;
		}
	}

	public void EditMed(View v) {
		String a = EditMName.getText().toString().trim();
		String b = EditMDes.getText().toString().trim();
		String c = EditMCount.getText().toString().trim();
		String e = EditMStartDate2.getText().toString().trim();
        String g = EditMDosage.getText().toString().trim();
		String h = "";
		if(mySpinner.getSelectedItem().toString()==Istrings[0]||mySpinner.getSelectedItem().toString()==Istrings[1])
		{
			h = c+"mg";
		}
		else if(mySpinner.getSelectedItem().toString()==Istrings[2])
		{
			h = c+"ml";
		}
		else if(mySpinner.getSelectedItem().toString()==Istrings[3])
		{
			h = c+"cc";
		}

        if(c.equals("0")||c.equals("00")||c.equals("000")||c.equals("0000")||c.equals("00000"))
        {
            Toast.makeText(this, "Stock cannot be 0", Toast.LENGTH_SHORT).show();
        }
 		else if(TextUtils.isEmpty(a)||TextUtils.isEmpty(b)||TextUtils.isEmpty(c)||TextUtils.isEmpty(e)||TextUtils.isEmpty(g))
        {
            Toast.makeText(this, "Please Insert All Fields", Toast.LENGTH_SHORT).show();
        }

 else {
		Medicine M = new Medicine();
		M.MED_ID = (int) id;
		M.NAME = EditMName.getText().toString();
		M.DESCRIPTION = EditMDes.getText().toString();
		M.STARTDATE = EditMStartDate.getText().toString();
		M.DOSAGE = h;
		//M.DOSAGE = EditMDosage.getText().toString();

		try {
			M.COUNT = Integer.parseInt(EditMCount.getText().toString());
		} catch (Exception ex) {
			M.COUNT = 0;
		}
		M.TIME1 = EditMTime1.getText().toString();


		M.STARTDATE2 = EditMStartDate2.getText().toString();

		M.IMAGE = mySpinner.getSelectedItemPosition() + 1;

		ML = new MedList(this.getBaseContext());
		ML.openWritable();
		boolean edit = ML.editMed(M);
		ML.close();

		if (edit) {
			SetReminders.Set(this, M);

			Intent intent = new Intent(this, TrackerActivity.class);
			intent.putExtra("from", "1");
			startActivity(intent);
		}
		this.finish();
	}
}

	public void showDatePickerDialog(View v) {
		if(v==EditMStartDate) {
			EditMStartDate.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					DialogFragment newFragment = new DatePickerFragment();
					newFragment.show(getFragmentManager(), "datePicker");
				}
			});
		}
		else if(v==EditMStartDate2) {
			EditMStartDate2.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					DialogFragment newFragment = new DatePickerFragment2();
					newFragment.show(getFragmentManager(), "datePicker");
				}
			});
		}
	}

	//@SuppressLint("ValidFragment")
	public static class DatePickerFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			EditText et = (EditText) getActivity().findViewById(
					R.id.EditMStartDate);
			String time = et.getText().toString();
			if (!time.isEmpty()) {
				try {
					String[] sp = time.split("/");
					year = Integer.parseInt(sp[2]);
					month = Integer.parseInt(sp[1]) - 1;
					day = Integer.parseInt(sp[0]);
				} catch (Exception ex) {
				}
			}
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			String a = String.format("%02d", day);
			String b = String.format("%02d", month + 1);
			String c = String.format("%04d", year);
			EditText dateValue = (EditText) getActivity().findViewById(
					R.id.EditMStartDate);
			dateValue.setText(a + "/" + b + "/" + c);
		}
	}

	//@SuppressLint("ValidFragment")
	public static class DatePickerFragment2 extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			EditText et = (EditText) getActivity().findViewById(
					R.id.EditMStartDate2);
			String time = et.getText().toString();
			if (!time.isEmpty()) {
				try {
					String[] sp = time.split("/");
					year = Integer.parseInt(sp[2]);
					month = Integer.parseInt(sp[1]) - 1;
					day = Integer.parseInt(sp[0]);
				} catch (Exception ex) {
				}
			}
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		@Override
		public void onDateSet(DatePicker view, int year, int month, int day) {
			String a = String.format("%02d", day);
			String b = String.format("%02d", month + 1);
			String c = String.format("%04d", year);
			EditText dateValue = (EditText) getActivity().findViewById(
					R.id.EditMStartDate2);
			dateValue.setText(a + "/" + b + "/" + c);
		}
	}

	public void showTimePickerDailog(View v) {
		DialogFragment newFragment = new TimePickerFragment(v.getId());
		newFragment.show(getFragmentManager(), "timePicker");
	}

	@SuppressLint("ValidFragment")
	public static class TimePickerFragment extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {

		int ID;
		int CheckID;

		public TimePickerFragment(int id) {
			ID = id;
			switch (ID) {
			case R.id.EditMTime1:
				//CheckID = R.id.EditMTime1Check;
				break;
			default:
				//CheckID = R.id.EditMTime1Check;
				break;
			}
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			int hour = 0;
			int min = 0;
			EditText et = (EditText) getActivity().findViewById(ID);
			String time = et.getText().toString();
			if (!time.isEmpty()) {
				try {
					String[] sp = time.split(":");
					hour = Integer.parseInt(sp[0]);
					min = Integer.parseInt(sp[1]);
				} catch (Exception ex) {
				}
			} else {
				final Calendar c = Calendar.getInstance();
				hour = c.get(Calendar.HOUR_OF_DAY);
				min = c.get(Calendar.MINUTE);
			}
			return new TimePickerDialog(getActivity(), this, hour, min, true);
		}

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			String a = String.format("%02d", hourOfDay);
			String b = String.format("%02d", minute);
			EditText dateValue = (EditText) getActivity().findViewById(ID);
			dateValue.setText(a + ":" + b);
			Log.e("AAAAA", "onTimeSet: " + a + " " + b );
			CheckBox cb = (CheckBox) getActivity().findViewById(CheckID);
			cb.setChecked(true);
		}

	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, TrackerActivity.class);
		startActivity(intent);
		this.finish();
		super.onBackPressed();
	}
}
