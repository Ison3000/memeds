package com.memeds.itproject2.helper;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;

import com.memeds.itproject2.MedicineActivity;
import com.memeds.itproject2.db.MedList;
import com.memeds.itproject2.obj.Medicine;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@SuppressLint({ "NewApi", "SimpleDateFormat", "ShowToast" })
public class SetReminders {
	static MedList ML;
	static Medicine medicine;
	static Context context;
	static long oneDayinMilli = 86400000;
	public static void Set(Context mContext, Medicine m) {
		context = mContext;
		medicine = m;
		for(int i=1;i<=6;i++){
			String sTime = getTimeString(i);
        	
			int tid = Integer.parseInt(String.valueOf(medicine.MED_ID)+"000"+i);
			Intent intent = new Intent(context, ReminderReceiver.class);
			intent.putExtra("id", (long)tid);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, tid, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			AlarmManager am = (AlarmManager) context.getSystemService(Service.ALARM_SERVICE);
			if (sTime != null && !sTime.isEmpty()){
	        	try{
	        	String string1 = medicine.STARTDATE + " " + sTime + ":00";
	            Date sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa").parse(string1);
	            Calendar calendarTime = Calendar.getInstance();
	            calendarTime.setTime(sdf);

	            Calendar calendar2 = Calendar.getInstance();
	            Date now = calendar2.getTime();

					String string2 = medicine.STARTDATE2 + " " + sTime + ":00";

					Date sdf2 = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa").parse(string2);
					Calendar calendarTime2 = Calendar.getInstance();
					calendarTime2.setTime(sdf2);


					if (now.before(calendarTime.getTime()))
					{
					am.setRepeating(AlarmManager.RTC_WAKEUP, calendarTime.getTimeInMillis(),oneDayinMilli, pendingIntent);
	            	}
					/*else if(now.before(calendarTime2.getTime()))
					{
						am.setRepeating(AlarmManager.RTC_WAKEUP, calendarTime.getTimeInMillis(),oneDayinMilli, pendingIntent);
					}*/

					else{
	            	String[] tim = sTime.split(":");
	            	Calendar cal = Calendar.getInstance();
	            	cal.set(Calendar.HOUR_OF_DAY,Integer.parseInt(tim[0]));
	            	cal.set(Calendar.MINUTE,Integer.parseInt(tim[1]));
	            	cal.set(Calendar.SECOND,0);
	            	cal.set(Calendar.MILLISECOND,0);
	            	am.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis()+oneDayinMilli,oneDayinMilli, pendingIntent);
	            }
	        	}catch(Exception ex){
	        		
	        	}		
			}else{
				pendingIntent.cancel();
				am.cancel(pendingIntent);
			}			
		}		
	}
	
	public static void Remove(MedicineActivity mContext, Medicine m) {
		medicine = m;
		for(int i=1;i<=6;i++){
			int tid = Integer.parseInt(String.valueOf(medicine.MED_ID)+"000"+i);
			Intent intent = new Intent(mContext, ReminderReceiver.class);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, tid, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			AlarmManager am = (AlarmManager) mContext.getSystemService(Service.ALARM_SERVICE);
			pendingIntent.cancel();
			am.cancel(pendingIntent);						
		}
	}
	
	public static String getTimeString(int i){
		switch (i) {
		case 1:	return medicine.TIME1;
		case 2:	return medicine.TIME2;
			case 3:	return medicine.TIME3;

			case 4:	return medicine.TIME4;

			case 5:	return medicine.TIME5;

			case 6:	return medicine.TIME6;
		default:return "";
		}		
	}
}
