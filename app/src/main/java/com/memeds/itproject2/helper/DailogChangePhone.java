package com.memeds.itproject2.helper;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.memeds.itproject2.R;
import com.memeds.itproject2.SettingsActivity;

@SuppressLint({ "InflateParams", "NewApi" })
public class DailogChangePhone {
    static SettingsActivity context;
    static TextView NewPass;

    public static void app_launched(SettingsActivity mContext) {
        context = mContext;
        final Dialog dialog = new Dialog(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.changephone, null);
        NewPass = (TextView) v.findViewById(R.id.CPhoneNewPhone);

        Button BtnClose = (Button) v.findViewById(R.id.CPhoneCancel);
        BtnClose.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button BtnChange = (Button) v.findViewById(R.id.CPhoneChange);
        BtnChange.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String newp = NewPass.getText().toString();
                if (!newp.isEmpty()) {
                    SharedPreferences pref = context.getApplicationContext()
                            .getSharedPreferences("UserDetails",
                                    Context.MODE_PRIVATE);
                        Editor editor = pref.edit();
                        editor.putString("RPhone", newp);
                        editor.commit();
                        Toast.makeText(context,
                                "Emergency Contact No Save",
                                Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                } else {
                    Toast.makeText(context, "Enter Emergency Contact No",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
        // WindowManager.LayoutParams.MATCH_PARENT);
        // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(v);
        dialog.setCancelable(false);
        dialog.show();
    }
}
